FROM alpine

ARG KUBECTL_VERSION=v1.25.4
ARG HELM_VERSION=3.10.2
ARG KUBEDOG_VERSION=0.9.6
ARG SOPS_VERSION=3.7.3
ARG HELMFILE=0.148.1
ARG AWS_IAM_AUTHENTICATOR_VERSION=0.5.9

ENV KUBECTL_VERSION=$KUBECTL_VERSION
ENV HELM_VERSION=$HELM_VERSION
ENV HELM_HOME=/helm/
ENV YC_HOME=/yc

ENV PATH $HELM_HOME:$YC_HOME/bin:$PATH

RUN apk --no-cache add \
	curl \
	python3 \
	py3-pip \
	py-crcmod \
	bash \
	libc6-compat \
	openssh-client \
	git \
	gnupg \
	jq \
	ca-certificates

RUN curl -LO https://dl.k8s.io/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
	chmod +x ./kubectl && \
	mv ./kubectl /usr/local/bin/kubectl


RUN curl -LO https://github.com/helmfile/helmfile/releases/download/v${HELMFILE}/helmfile_${HELMFILE}_linux_amd64.tar.gz && \
	tar xzf helmfile_${HELMFILE}_linux_amd64.tar.gz && \
	mv helmfile /usr/local/bin/helmfile

RUN curl -LO https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz && \
	tar xzf helm-v${HELM_VERSION}-linux-amd64.tar.gz && \
	mv linux-amd64 $HELM_HOME && \
	rm helm-v${HELM_VERSION}-linux-amd64.tar.gz && \
	mkdir -p $HELM_HOME/plugins && \
	helm plugin install https://github.com/futuresimple/helm-secrets

RUN pip3 install --upgrade pip \
	&& pip3 install awscli \
	&& rm -rf /var/cache/apk/*

RUN curl -Lo /usr/local/bin/aws-iam-authenticator https://github.com/kubernetes-sigs/aws-iam-authenticator/releases/download/v${AWS_IAM_AUTHENTICATOR_VERSION}/aws-iam-authenticator_${AWS_IAM_AUTHENTICATOR_VERSION}_linux_amd64 \
	&& chmod +x /usr/local/bin/aws-iam-authenticator


RUN curl -L -o /usr/local/bin/kubedog https://tuf.kubedog.werf.io/targets/releases/${KUBEDOG_VERSION}/linux-amd64/bin/kubedog \
	&& chmod +x /usr/local/bin/kubedog

RUN curl -L -o /usr/local/bin/sops https://github.com/mozilla/sops/releases/download/v${SOPS_VERSION}/sops-v${SOPS_VERSION}.linux \
	&& chmod 0755 /usr/local/bin/sops \
	&& chown root:root /usr/local/bin/sops

VOLUME ["/root/.config"]
